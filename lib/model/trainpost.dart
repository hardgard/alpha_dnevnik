
import 'package:firebase_database/firebase_database.dart';

class TrainPost{
  String key;
  String subject;//override
  String body;

  TrainPost(this.subject, this.body);

  TrainPost.fromSnapshot(DataSnapshot snapshot):
      key = snapshot.key,
      subject = snapshot.value["subject"],
      body = snapshot.value["body"];

  toJson(){
    return {
      "subject": subject,
      "body": body
    };
  }

}