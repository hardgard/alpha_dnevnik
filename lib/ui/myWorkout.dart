import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:pre_dnevnik_app/ui/addWorkout.dart';
import 'package:pre_dnevnik_app/ui/addWorkoutV2.dart';
import 'package:pre_dnevnik_app/ui/editWorkout.dart';
import 'package:pre_dnevnik_app/ui/login.dart';

class MyWorkout extends StatefulWidget {

  MyWorkout({this.user,this.googleSignIn});

  final FirebaseUser user;
  final GoogleSignIn googleSignIn;

  @override
  _MyWorkoutState createState() => _MyWorkoutState();
}

class _MyWorkoutState extends State<MyWorkout> {

  void _signOut(){
    AlertDialog alertDialog = new AlertDialog(
      content: Container(
        height: 255.0,
        child: Column(
          children: <Widget>[
            ClipOval(
              child: Image.network(widget.user.photoUrl),
            ),

            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text('SignOut?',style: TextStyle(fontSize: 16.0),),
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                InkWell(
                    onTap: (){
                      widget.googleSignIn.signOut();
                      Navigator.of(context).push(
                        MaterialPageRoute(builder: (BuildContext context)=> Login())
                      );
                    },
                    child: Column(
                  children: <Widget>[
                    Icon(Icons.check),
                    Padding(padding: EdgeInsets.all(5.0)),
                    Text('Yes'),
                  ],
                )),
                InkWell(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: Column(
                      children: <Widget>[
                        Icon(Icons.close),
                        Padding(padding: EdgeInsets.all(5.0)),
                        Text('Cancel'),
                      ],
                    )),
              ],
            )
          ],
        ),
      ),
    );
    showDialog(context: context,child: alertDialog );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          backgroundColor: Colors.amberAccent,
          onPressed: (){
            Navigator.of(context).push(
                new MaterialPageRoute(builder: (BuildContext context) =>  new AddWorkoutV2(email: widget.user.email,)));
            }),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        elevation: 20.0,
          color: Colors.amberAccent,
          child: ButtonBar(children: <Widget>[
             ],),),
      body: Stack(
        children: <Widget>[

          Padding(
            padding: const EdgeInsets.only(top: 160.0),
            child: StreamBuilder(
                stream: Firestore.instance.collection('Workout').where('email',isEqualTo: widget.user.email).snapshots(),
                builder: (BuildContext context,AsyncSnapshot<QuerySnapshot>snapshot){
                  if(!snapshot.hasData)
                    return new Container(child: Center(child: CircularProgressIndicator(),),);
                  return new TaskList(document: snapshot.data.documents,);
                }),
          ),

          Container(
            color: Colors.amberAccent,
            height: 170.0,
            width: double.infinity,
            child: Padding(
              padding: const EdgeInsets.all(18.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        width: 60.0,
                        height: 60.0,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(image: NetworkImage(widget.user.photoUrl),
                          fit: BoxFit.cover)
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 18.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text('Welcome',style: TextStyle(fontSize: 18.0, color: Colors.white)),
                              Text(widget.user.displayName,style: TextStyle(fontSize: 24.0, color: Colors.white)),
                            ],
                          ),
                        ),
                      ),
                      IconButton(icon: Icon(Icons.exit_to_app,color: Colors.white,size: 30.0),
                          onPressed: ()=> _signOut())
                    ],
                  ),
                  Text('My Workout', style: TextStyle(fontSize: 30.0, color: Colors.white,letterSpacing: 2.0), )
                ],
              ),
            ),

          ),
        ],
      ),
    );
  }
  }
  class TaskList extends StatelessWidget {


  TaskList({this.document});
  final List<DocumentSnapshot> document;
    @override
    Widget build(BuildContext context) {
      return ListView.builder(itemBuilder: (BuildContext context, int i){
        String title = document[i].data['title'].toString();
        String note = document[i].data['note'].toString();
        DateTime _date = document[i].data['date'];
        String duedate = '${_date.day}/${_date.month}/${_date.year}';
        return Dismissible(
          key: Key(document[i].documentID),
          onDismissed: (direction){
            Firestore.instance.runTransaction((transaction)async{
              DocumentSnapshot snapshot = await transaction.get(document[i].reference);
              await transaction.delete(snapshot.reference);
            });
            Scaffold.of(context).showSnackBar(SnackBar(content: Text('workout deleted')));
          },
          child: Padding(
            padding: const EdgeInsets.only(left:16.0,top: 8.0,right: 16.0,bottom: 8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Container(child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(bottom:8.0),
                        child: Text(title, style: TextStyle(fontSize: 20.0,letterSpacing: 1.0),),
                      ),
                      Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(right: 16.0),
                            child: Icon(Icons.date_range,color: Colors.amber,),
                          ),
                          Text(duedate, style: TextStyle(fontSize: 18.0,),),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(right: 16.0),
                            child: Icon(Icons.note,color: Colors.amber,),
                          ),
                          Expanded(child: Text(note, style: TextStyle(fontSize: 18.0,),)),
                        ],
                      )
                    ],
                  ),),
                ),
                IconButton(icon: Icon(Icons.edit,color: Colors.black45,), onPressed: (){
                  Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context)=> EditWorkout(
                    title: title,
                    duedate:  document[i].data['date'],
                    note: note,
                    index: document[i].reference,
                  )));
                })
              ],
            ),
          ),
        );
      },
      itemCount: document.length,);
    }
  }
  

