import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';


class AddWorkout extends StatefulWidget {

  AddWorkout({this.email});
  final String email;

  @override
  _AddWorkoutState createState() => _AddWorkoutState();
}

class _AddWorkoutState extends State<AddWorkout> {

  DateTime _dueDate = new DateTime.now();
  String _dateText='';
  String newWorkout = '';
  String newNote = '';

  Future<Null> _selectDueDate(BuildContext context) async{
    final picked = await showDatePicker(context: context, initialDate: _dueDate, firstDate: DateTime(2018), lastDate: DateTime(2100));
    if(picked != null){
      setState(() {
        _dueDate = picked;
        _dateText='${picked.day}/${picked.month}/${picked.year}';
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _dateText='${_dueDate.day}/${_dueDate.month}/${_dueDate.year}';
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(
        children: <Widget>[
          Container(height: 200.0,
          width: double.infinity,
          decoration: BoxDecoration(color: Colors.redAccent),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
            Text('My Workout', style: TextStyle(fontSize: 30.0, color: Colors.white,letterSpacing: 2.0), ),
            Padding(
              padding: const EdgeInsets.only(top: 20.0),
              child: Text('Add Task', style: TextStyle(fontSize: 24.0, color: Colors.white,letterSpacing: 2.0), ),
            ),
              Icon(Icons.list,color: Colors.white, size: 30.0,)
          ],),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: TextField(
              onChanged: (String str){
                setState(() {
                  newWorkout = str;
                });
              },
              decoration: InputDecoration(icon: Icon(Icons.dashboard),hintText: 'New workout',border: InputBorder.none),style: TextStyle(fontSize: 22.0,color: Colors.black),),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              children: <Widget>[
                Icon(Icons.date_range),
                Expanded(child: Text('Due Date', style: TextStyle(fontSize: 22.0, color: Colors.black,letterSpacing: 2.0),)),
                FlatButton(
                    onPressed: ()=>_selectDueDate(context),
                    child: Text(_dateText, style: TextStyle(fontSize: 22.0, color: Colors.black,letterSpacing: 2.0),)),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: TextField(
              onChanged: (String str){
                setState(() {
                  newNote = str;
                });
              },
              decoration: InputDecoration(icon: Icon(Icons.note),hintText: 'Note',border: InputBorder.none),style: TextStyle(fontSize: 22.0,color: Colors.black),),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 100.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                IconButton(icon: Icon(Icons.check,size: 40.0), onPressed: (){
                  _addData();
                }),
                IconButton(icon: Icon(Icons.close,size: 40.0), onPressed: (){Navigator.pop(context);})
              ],
            ),
          )
        ],
      ),
    );
  }

  void _addData() {
    Firestore.instance.runTransaction((Transaction transaction) async{
      CollectionReference reference = Firestore.instance.collection('Workout');
      await reference.add({
          "email": widget.email,
        "title": newWorkout,
        'date': _dueDate,
        "note": newNote
      });
    });
    Navigator.pop(context);
  }
}
