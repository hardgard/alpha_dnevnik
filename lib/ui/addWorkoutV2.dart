import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class AddWorkoutV2 extends StatefulWidget {
  AddWorkoutV2({this.email});

  final String email;

  @override
  _AddWorkoutV2State createState() => _AddWorkoutV2State();
}

class _AddWorkoutV2State extends State<AddWorkoutV2> {
  int setNum = 5;
  ObjectKey key;//??
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
        appBar: AppBar(
          title: Text('New Workout'),
          backgroundColor: Colors.amberAccent,
        ),
    body: Center(
      child: Container(
        child: ListView.builder(itemCount: 5,
          scrollDirection: Axis.vertical,
          itemBuilder: (BuildContext context, int index){
          return addExercize3();
          },
        ),
      ),
    ),
        bottomNavigationBar: BottomAppBar(elevation: 16.0,
        color: Colors.amberAccent,
        child: ButtonBar(
          alignment: MainAxisAlignment.start,
          children: <Widget>[
            FlatButton(onPressed: (){
            }
            , child: Text('add exercize'))
          ],
        ),),
    ));

 }

Widget addExercize3(){

  return  Container(key: key,
      margin: EdgeInsets.symmetric(vertical: 20.0),
      height: 250.0,
      child:  Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(child: TextField( decoration: InputDecoration(icon: Icon(Icons.dashboard),hintText: 'New exercize',border: InputBorder.none),style: TextStyle(fontSize: 16.0,color: Colors.black),)),
                  FlatButton(onPressed: (){
                    setState(() {
                      setNum++;
                    });
                  }, child: Text('add set'))
                ],
              ),
            //  Padding(
            //    padding: const EdgeInsets.only(top:8.0),
            //    child: TextField( decoration: InputDecoration(icon: Icon(Icons.dashboard),hintText: 'New exercize',border: InputBorder.none),style: TextStyle(fontSize: 16.0,color: Colors.black),),
            //  ),
              Divider(height: 1.0, color: Colors.red,),
              SizedBox.fromSize(size: Size.fromHeight(120.0),
                child: ListView.builder(itemCount: setNum,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (BuildContext context, int index){
                  int setCount = 1+index;
                      return Row(
                        children: <Widget>[
                          Container(height: 130.0,width: 100.0, color: Colors.redAccent,
                            child: Column(
                              children: <Widget>[
                                TextField(decoration: InputDecoration(labelText: setCount.toString()),),
                                TextField(decoration: InputDecoration(labelText: setCount.toString()),)
                              ],
                            ),
                          )
                        ],
                      );
                  },

          ),
              )
        ],
      )
  );}}


Widget addExercize2(){
  return  Container(
      margin: EdgeInsets.symmetric(vertical: 20.0),
      height: 200.0,
      child:  ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          Container(width: 160.0, color: Colors.red,),
          Container(width: 160.0, color: Colors.orange,),
          Container(width: 160.0, color: Colors.pink,),
          Container(width: 160.0, color: Colors.yellow,),
        ],
      )
  );}

//      Widget addExercize() {
//
//
//
//        return Expanded(
//          child: Padding(
//            padding: const EdgeInsets.only(top:3.0),
//            child: Center(
//              child: Column(
//                mainAxisAlignment: MainAxisAlignment.start,
//                children: <Widget>[
//                  Padding(
//                    padding: const EdgeInsets.all(1.0),
//                    child: TextField(
//                      onChanged: (String str){ setState(() {                           newWorkout = str;
//                        });
//                      },
//                      decoration: InputDecoration(icon: Icon(Icons.dashboard),hintText: 'New exercize',border: InputBorder.none),style: TextStyle(fontSize: 16.0,color: Colors.black),),
//                  ),
//                  Divider(height: 1.0, color: Colors.red,),
//                  Text("Weights"),
//                  SizedBox.fromSize(size: Size.fromHeight(120.0),
//                    child: ListView(scrollDirection: Axis.horizontal,
//                      children: <Widget>[
//                        ListView.builder(itemCount: setNum,
//                            shrinkWrap: true,
//                            scrollDirection: Axis.horizontal,
//                            itemBuilder: (BuildContext context, int index) {
//                              int setCount = 1+index;//count of sets
//                              return Row(
//                                children: <Widget>[
//                                  Container(height: 100.0,
//                                    width: 100.0,
//                                    child: Column(
//                                      mainAxisAlignment: MainAxisAlignment.start,
//                                      children: <Widget>[
//                                        Expanded(child: TextField(
//                                          decoration: InputDecoration(
//                                              labelText: setCount.toString(),
//                                              border: InputBorder.none),
//                                          style: TextStyle(
//                                              fontSize: 16.0, color: Colors.black),)),
//                                        Expanded(child: TextField(
//                                          decoration: InputDecoration(
//                                            labelText: setCount.toString(),
//
//                                          ),
//                                          style: TextStyle(
//                                              fontSize: 16.0, color: Colors.black),)),
//
//                                      ],
//                                    ),
//                                  ),
//                                ],
//                              );
//                            }),
//                        FlatButton(onPressed: (){
//                          setState(() {
//                            setNum++;
//                          });
//                        },
//                            child: Icon(Icons.add))
//                      ],
//                    ),
//                  ),
//                  Text("Reps"),
//                ],
//              ),),
//          ),
//        );
//      }
//    }

