import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:pre_dnevnik_app/ui/myWorkout.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;
final GoogleSignIn _googleSignIn = new GoogleSignIn();

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {

  String _imageUrl;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("login"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: FlatButton(
                  onPressed: () => _gSignin(),
                  child: Text('Google SignIn'),
              color: Colors.red,),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: FlatButton(
                onPressed: () => _signInWithEmail(),
                child: Text('Signin with Email'),
                color: Colors.orange)
            ),

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: FlatButton(
                onPressed: () =>  _createUser(),
                child: Text('Create Account'),
                color: Colors.purpleAccent,),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: FlatButton(
                  color: Colors.redAccent,
                  onPressed: ()=>_logout(),
                  child: Text("LogOut")),
            ),
            Expanded(child: Image.network(_imageUrl == null || _imageUrl.isEmpty ? "http://www.nokiaplanet.com/uploads/posts/2015-11/1448881894_cute-kitty-360x640.jpg" : _imageUrl))
          ],
        ),
      ),
    );
  }

  Future <FirebaseUser> _gSignin() async {
    GoogleSignInAccount googleSignInAccount = await _googleSignIn.signIn();
    GoogleSignInAuthentication googleSignInAuthentication = await googleSignInAccount.authentication;

    FirebaseUser user = await _auth.signInWithGoogle(idToken: googleSignInAuthentication.idToken, accessToken: googleSignInAuthentication.accessToken);

    Navigator.of(context).push(
      new MaterialPageRoute(builder: (BuildContext context)=> new MyWorkout(user: user,googleSignIn: _googleSignIn))
    );

    return user;

  }

  Future _createUser() async{
    FirebaseUser user = await _auth.createUserWithEmailAndPassword(
        email: "neonLama@gmail.com", password: "test1234").then((userNew){
          print("user created ${userNew.email}");
    });
  }

  _logout() {
    setState(() {
      _googleSignIn.signOut();
      _imageUrl = null;
    });

  }

  _signInWithEmail() {
    _auth.signInWithEmailAndPassword(email: "neonLama@gmail.com", password: "test1234").catchError((error){
      print("something went wrong ${error.toString()}");
    })
        .then((newUser){
      print("User signed in with : ${newUser.email}");
    });
  }
}
