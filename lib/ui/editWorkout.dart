import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';


class EditWorkout extends StatefulWidget {

  EditWorkout({this.title,this.duedate,this.note,this.index});

  final String title;
  final DateTime duedate;
  final String note;
  final index;

  @override
  _EditWorkoutState createState() => _EditWorkoutState();
}

class _EditWorkoutState extends State<EditWorkout> {

  TextEditingController controllerTitle;
  TextEditingController controllerNote;


  DateTime _dueDate ;
  String _dateText='';
  String newWorkout;
  String newNote;

  Future<Null> _selectDueDate(BuildContext context) async{
    final picked = await showDatePicker(context: context, initialDate: _dueDate, firstDate: DateTime(2018), lastDate: DateTime(2100));
    if(picked != null){
      setState(() {
        _dueDate = picked;
        _dateText='${picked.day}/${picked.month}/${picked.year}';

      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _dueDate=widget.duedate;
    _dateText='${_dueDate.day}/${_dueDate.month}/${_dueDate.year}';
    newWorkout = widget.title;
    newNote=widget.note;
    controllerTitle  = new TextEditingController(text: widget.title);
    controllerNote  = new TextEditingController(text: widget.note);
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(
        children: <Widget>[
          Container(height: 200.0,
            width: double.infinity,
            decoration: BoxDecoration(color: Colors.redAccent),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('My Workout', style: TextStyle(fontSize: 30.0, color: Colors.white,letterSpacing: 2.0), ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: Text('Edit Task', style: TextStyle(fontSize: 24.0, color: Colors.white,letterSpacing: 2.0), ),
                ),
                Icon(Icons.list,color: Colors.white, size: 30.0,)
              ],),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: TextField(
              controller: controllerTitle,
              onChanged: (String str){
                setState(() {
                  newWorkout = str;
                });
              },
              decoration: InputDecoration(icon: Icon(Icons.dashboard),hintText: 'New workout',border: InputBorder.none),style: TextStyle(fontSize: 22.0,color: Colors.black),),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              children: <Widget>[
                Icon(Icons.date_range),
                Expanded(child: Text('Due Date', style: TextStyle(fontSize: 22.0, color: Colors.black,letterSpacing: 2.0),)),
                FlatButton(
                    onPressed: ()=>_selectDueDate(context),
                    child: Text(_dateText, style: TextStyle(fontSize: 22.0, color: Colors.black,letterSpacing: 2.0),)),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: TextField(
              controller: controllerNote,
              onChanged: (String str){
                setState(() {
                  newNote = str;
                });
              },
              decoration: InputDecoration(icon: Icon(Icons.note),hintText: 'Note',border: InputBorder.none),style: TextStyle(fontSize: 22.0,color: Colors.black),),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 100.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                IconButton(icon: Icon(Icons.check,size: 40.0), onPressed: (){
                  _editData();
                }),
                IconButton(icon: Icon(Icons.close,size: 40.0), onPressed: (){Navigator.pop(context);})
              ],
            ),
          )
        ],
      ),
    );
  }

  void _editData() {
    Firestore.instance.runTransaction((Transaction transaction) async{
      DocumentSnapshot snapshot = await transaction.get(widget.index);
      await transaction.update(snapshot.reference, {
        "title": newWorkout,
        'date': _dueDate,
        "note": newNote
      });
         });
    Navigator.pop(context);
  }


}
