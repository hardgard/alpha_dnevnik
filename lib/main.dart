import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:pre_dnevnik_app/model/trainpost.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:google_sign_in/google_sign_in.dart';
import './ui/login.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        title: 'Alpha dnevnik',
        theme: new ThemeData(
            primarySwatch: Colors.blue,
            primaryColor: defaultTargetPlatform == TargetPlatform.iOS ? Colors
                .blueGrey : null
        ),
        home: Login(),
        //routes: <String, WidgetBuilder>{
        //    "/a":(BuildContext context)=> Login(),}
    );
  }
}

class MyHomePage extends StatefulWidget {
  final DocumentReference fireDoc = Firestore.instance.document("myData/workout");

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<TrainPost> tpostMessages = List();
  TrainPost tpost;
  final FirebaseDatabase database = FirebaseDatabase.instance;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  DatabaseReference databaseReference;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    tpost = TrainPost("", "");
    databaseReference = database.reference().child("workout");
    databaseReference.onChildAdded.listen(_onEntryAdded);
    databaseReference.onChildChanged.listen(_onEntryChanged);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Workout"),
        elevation: defaultTargetPlatform == TargetPlatform.android ? 5.0 : 0.0,
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: Text('HardGard'),
              accountEmail: Text('hardgard@mail.ru'),
            ),
            ListTile(
              title: Text('Login'),
              trailing: Icon(Icons.accessibility),
              onTap: (){
                Navigator.of(context).pop();
                //Navigator.of(context).pushNamed("/a");
                Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context)=>Login()));
              },
            ),
            ListTile(
              title: Text('Page 2'),
              trailing: Icon(Icons.arrow_forward),
            ),
            Divider(),
            ListTile(
              title: Text('Close'),
              trailing: Icon(Icons.exit_to_app),
              onTap: () => Navigator.of(context).pop(),
            )
          ],
        ),
      ),
      body: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Flexible(
              flex: 0,
              child: Center(
                child: Form(
                  key: formKey,
                  child: Flex(
                    direction: Axis.vertical,
                    children: <Widget>[
                      ListTile(
                        leading: Icon(Icons.subject),
                        title: TextFormField(
                          initialValue: "",
                          onSaved: (val) => tpost.subject = val,
                          validator: (val) => val == "" ? val : null,
                        ),
                      ),
                      ListTile(
                        leading: Icon(Icons.message),
                        title: TextFormField(
                          initialValue: "",
                          onSaved: (val) => tpost.body = val,
                          validator: (val) => val == "" ? val : null,
                        ),
                      ),
                      FlatButton(
                          onPressed: () {
                            handleSubmit();
                          },
                          color: Colors.redAccent,
                          child: Text("post"))
                    ],
                  ),
                ),
              )),
          Flexible(
              child: FirebaseAnimatedList(
                query: databaseReference,
                itemBuilder: (_, DataSnapshot snapshot,
                    Animation<double> animation,
                    int index) {
                  return new Card(
                    child: ListTile(
                      leading: CircleAvatar(
                        backgroundColor: Colors.red,
                      ),
                      title: Text(tpostMessages[index].subject),
                      subtitle: Text(tpostMessages[index].body),
                    ),
                  );
                },
              ))
        ],
      ),
    );
  }

  void _onEntryAdded(Event event) {
    setState(() {
      tpostMessages.add(TrainPost.fromSnapshot(event.snapshot));
    });
  }

  void handleSubmit() {
    final FormState form = formKey.currentState;
    if (form.validate()) {
      form.save();
      form.reset();
      //save form to the db
      databaseReference.push().set(tpost.toJson());
    }
  }

  void _onEntryChanged(Event event) {
    var oldEntry = tpostMessages.singleWhere((entry) {
      return entry.key == event.snapshot.key;
    });
    setState(() {
      tpostMessages[tpostMessages.indexOf(oldEntry)] =
          TrainPost.fromSnapshot(event.snapshot);
    });
  }
}
